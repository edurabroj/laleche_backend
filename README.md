#Instrucciones#

1. Clonar repo
2. Abrir terminal en el directorio del proyecto
3. Ejecutar "npm i"
4. Ejecutar "npm run dev"

#Acopio de leche API#
##Base URL##
http://localhost:8001/api/v1/

#Fábricas#
####Listar####
GET: /fabricas

####Obtener por código ####
GET: /fabricas/:codigo

####Crear####
POST: /fabricas

####Editar####
PUT: /fabricas

####Eliminar####
DELETE: /fabricas/:codigo

#Proveedores#
####Listar####
GET: /proveedores

####Listar por recolector ####
GET: /proveedores/byRecolector/:codigoRecolector

####Obtener por código ####
GET: /proveedores/:codigo

####Crear####
POST: /proveedores

####Editar####
PUT: /proveedores

####Eliminar####
DELETE: /proveedores/:codigo

#Recolectores#
####Listar####
GET: /recolectores

####Listar por fábrica ####
GET: /recolectores/byFabrica/:codigoFabrica

####Obtener por código ####
GET: /recolectores/:codigo

####Crear####
POST: /recolectores

####Editar####
PUT: /recolectores

####Eliminar####
DELETE: /recolectores/:codigo

#Acopios#
####Listar por recolector, fecha y turno####
GET: /acopios/:recolectorCod/:fecha/:turno

- Turno: 'M' o 'T'
