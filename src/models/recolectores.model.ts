import { Table, Column, Model, HasMany } from "sequelize-typescript";

@Table
export default class Recolectores extends Model<Recolectores> {
  @Column({ primaryKey: true })
  Codigo: number;

  @Column
  dni: string;

  @Column
  nombres: string;

  @Column
  apPaterno: string;

  @Column
  apMaterno: string;

  @Column
  celular: string;
}
