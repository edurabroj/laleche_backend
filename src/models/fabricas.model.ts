import {
  Table,
  Column,
  Model,
  HasMany,
  AutoIncrement
} from "sequelize-typescript";

@Table
export default class Fabricas extends Model<Fabricas> {
  @AutoIncrement
  @Column({ primaryKey: true })
  Codigo: number;

  @Column
  Nombre: string;
}
