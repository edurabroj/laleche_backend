import {
  Table,
  Column,
  Model,
  HasMany,
  AutoIncrement
} from "sequelize-typescript";

@Table
export default class Acopios extends Model<Acopios> {
  @Column({ primaryKey: true })
  Recolector: number;

  @Column({ primaryKey: true })
  Proveedor: number;

  @Column({ primaryKey: true })
  Fecha: Date;

  @Column({ primaryKey: true })
  Turno: string;

  @Column
  Cantidad: number;
}
