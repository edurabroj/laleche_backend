import { Table, Column, Model, HasMany } from "sequelize-typescript";

@Table
export default class Proveedores extends Model<Proveedores> {
  @Column({ primaryKey: true })
  codigo: number;

  @Column
  dni: string;

  @Column
  nombres: string;

  @Column
  apPaterno: string;

  @Column
  apMaterno: string;

  @Column
  celular: string;

  @Column
  distrito: string;
}
