import { Sequelize } from "sequelize-typescript";

const env = process.env.NODE_ENV || "development";
const config = require("./config/config.json")[env];

export const sequelize = new Sequelize({
  database: config.database,
  dialect: "mssql",
  username: config.username,
  password: config.password,
  modelPaths: [__dirname + "/models"],
  modelMatch: (filename, member) => {
    return (
      filename.substring(0, filename.indexOf(".model")) === member.toLowerCase()
    );
  }
});
