import Proveedor from "../models/proveedores.model";
import Acopios from "../models/acopios.model";
const { sequelize } = require("../sequelize");

export default class AcopioService extends Acopios {
  static findByRecolectorFechaTurno(
    recolectorId: number,
    fecha: Date,
    turno: string
  ) {
    return sequelize.query(
      "EXEC SP_AcopiosByRecolectorFechaTurno @recolector = ?, @fecha = ?, @turno = ?",
      {
        replacements: [recolectorId, fecha, turno],
        type: sequelize.QueryTypes.SELECT
      }
    );
  }
}
