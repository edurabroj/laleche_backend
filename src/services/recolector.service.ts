import Recolectores from "../models/recolectores.model";
const { sequelize } = require("../sequelize");

export default class RecolectorService extends Recolectores {
  static findByFabrica(fabricaId: number) {
    return sequelize.query("EXEC SP_RecolectoresByFabrica @Fabrica = ?;", {
      replacements: [fabricaId],
      type: sequelize.QueryTypes.SELECT
    });
  }
}
