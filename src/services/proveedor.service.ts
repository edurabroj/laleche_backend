import Proveedor from "../models/proveedores.model";
const { sequelize } = require("../sequelize");

export default class ProveedorService extends Proveedor {
  static findByRecolector(recolectorId: number) {
    return sequelize.query("EXEC SP_ProveedoresByRecolector @Recolector = ?;", {
      replacements: [recolectorId],
      type: sequelize.QueryTypes.SELECT
    });
  }
}
