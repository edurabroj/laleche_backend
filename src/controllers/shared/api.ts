import { Request, Response } from "express";

export default class Api {
  static list(req: Request, res: Response, promise: any) {
    promise
      .then(data => {
        return res.json({
          ok: true,
          data
        });
      })
      .catch(err => {
        return res.status(500).json({
          ok: false,
          err: err.message
        });
      });
  }

  static one(req: Request, res: Response, promise: any) {
    promise
      .then(data => {
        return res.json({
          ok: true,
          data
        });
      })
      .catch(err => {
        return res.status(500).json({
          ok: false,
          err: err.message
        });
      });
  }

  static create(req: Request, res: Response, promise) {
    promise
      .then(created => {
        res.json({
          ok: true,
          created
        });
      })
      .catch(err => {
        return res.status(500).json({
          ok: false,
          err: err.message
        });
      });
  }

  static update(req: Request, res: Response, promise) {
    promise
      .then(updated => {
        res.json({
          ok: true,
          updated
        });
      })
      .catch(err => {
        return res.status(500).json({
          ok: false,
          err: err.message
        });
      });
  }

  static delete(req: Request, res: Response, promise) {
    promise
      .then(deleted => {
        res.json({
          ok: true,
          deleted
        });
      })
      .catch(err => {
        return res.status(500).json({
          ok: false,
          err: err.message
        });
      });
  }
}
