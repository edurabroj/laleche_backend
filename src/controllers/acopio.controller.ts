import Service from "../services/acopio.service";
import Api from "./shared/api";
import { Request, Response } from "express";

export default class AcopioController {
  static getByRecolectorFechaTurno(req: Request, res: Response) {
    const { recolector, fecha, turno } = req.params;
    Api.list(
      req,
      res,
      Service.findByRecolectorFechaTurno(recolector, fecha, turno)
    );
  }
}
