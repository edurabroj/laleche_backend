import Service from "../services/recolector.service";
import Api from "./shared/api";
import { Request, Response } from "express";

exports.getRecolectoresByFabrica = (req: Request, res: Response) => {
  Api.list(req, res, Service.findByFabrica(req.params.pk));
};

exports.getList = (req: Request, res: Response) => {
  Api.list(req, res, Service.findAll());
};

exports.get = (req: Request, res: Response) => {
  Api.one(req, res, Service.findById(req.params.pk));
};

exports.create = (req: Request, res: Response) => {
  Api.create(req, res, Service.create(req.body));
};

exports.update = (req: Request, res: Response) => {
  Api.update(
    req,
    res,
    Service.update(req.body, { where: { codigo: req.params.pk } })
  );
};

exports.delete = (req: Request, res: Response) => {
  Api.delete(req, res, Service.findById(req.params.pk));
};
