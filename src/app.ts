const express = require("express"),
  bodyParser = require("body-parser"),
  app = express(),
  router = require("./router/index"),
  cors = require("cors"),
  PORT = 8001;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const { sequelize } = require("./sequelize");

app.use("/", router);

app.listen(PORT, () => {
  console.log("Corriendo en " + PORT);
});
