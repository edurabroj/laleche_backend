import { Router } from "express";

const router = Router();
const fabricas = require("./routes/fabrica.routes");
const proveedores = require("./routes/proveedor.routes");
const recolectores = require("./routes/recolector.routes");
const acopios = require("./routes/acopio.routes");

router.use("/fabricas", fabricas);
router.use("/proveedores", proveedores);
router.use("/recolectores", recolectores);
router.use("/acopios", acopios);

module.exports = router;
