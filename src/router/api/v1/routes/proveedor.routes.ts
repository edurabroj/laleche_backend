const ProveedorController = require("../../../../controllers/proveedor.controller");
import { Request, Response, Router } from "express";

const router = Router();

router.get("/byRecolector/:pk", (req: Request, res: Response) => {
  ProveedorController.getProveedoresByRecolector(req, res);
});

router.get("/", (req: Request, res: Response) => {
  ProveedorController.getList(req, res);
});

router.get("/:pk", (req: Request, res: Response) => {
  ProveedorController.get(req, res);
});

router.post("/", (req: Request, res: Response) => {
  ProveedorController.create(req, res);
});

router.put("/:pk", (req: Request, res: Response) => {
  ProveedorController.update(req, res);
});

router.delete("/:pk", (req: Request, res: Response) => {
  ProveedorController.delete(req, res);
});

module.exports = router;
