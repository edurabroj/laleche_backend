const FabricaController = require("../../../../controllers/fabrica.controller");
import { Router, Request, Response } from "express";

const router = Router();

router.get("/", (req: Request, res: Response) => {
  FabricaController.getList(req, res);
});

router.get("/:pk", (req: Request, res: Response) => {
  FabricaController.get(req, res);
});

router.post("/", (req: Request, res: Response) => {
  FabricaController.create(req, res);
});

router.put("/:pk", (req: Request, res: Response) => {
  FabricaController.update(req, res);
});

router.delete("/:pk", (req: Request, res: Response) => {
  FabricaController.delete(req, res);
});

module.exports = router;
