const RecolectorController = require("../../../../controllers/recolector.controller");
import { Router, Request, Response } from "express";

const router = Router();

router.get("/byFabrica/:pk", (req: Request, res: Response) => {
  RecolectorController.getRecolectoresByFabrica(req, res);
});

router.get("/", (req: Request, res: Response) => {
  RecolectorController.getList(req, res);
});

router.get("/:pk", (req: Request, res: Response) => {
  RecolectorController.get(req, res);
});

router.post("/", (req: Request, res: Response) => {
  RecolectorController.create(req, res);
});

router.put("/:pk", (req: Request, res: Response) => {
  RecolectorController.update(req, res);
});

router.delete("/:pk", (req: Request, res: Response) => {
  RecolectorController.delete(req, res);
});

module.exports = router;
