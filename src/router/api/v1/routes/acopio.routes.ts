import AcopioController from "../../../../controllers/acopio.controller";
import { Router, Request, Response } from "express";

const router = Router();

router.get("/:recolector/:fecha/:turno", (req: Request, res: Response) => {
  AcopioController.getByRecolectorFechaTurno(req, res);
});

module.exports = router;
